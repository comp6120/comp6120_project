CREATE DATABASE  IF NOT EXISTS `comp6120_project` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `comp6120_project`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: comp6120_project
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administers`
--

DROP TABLE IF EXISTS `administers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administers` (
  `idAdministers` int(11) NOT NULL AUTO_INCREMENT,
  `Technician_Employees_Workers_wid` int(11) DEFAULT NULL,
  `Nurse_Employees_Workers_wid` int(11) DEFAULT NULL,
  `Doctor_Employees_Workers_wid` int(11) DEFAULT NULL,
  `Treatments_idTreatment` int(11) NOT NULL,
  PRIMARY KEY (`idAdministers`),
  KEY `fk_Administers_Technician1_idx` (`Technician_Employees_Workers_wid`),
  KEY `fk_Administers_Nurse1_idx` (`Nurse_Employees_Workers_wid`),
  KEY `fk_Administers_Doctor1_idx` (`Doctor_Employees_Workers_wid`),
  KEY `fk_Administers_Treatments1_idx` (`Treatments_idTreatment`),
  CONSTRAINT `fk_Administers_Doctor1` FOREIGN KEY (`Doctor_Employees_Workers_wid`) REFERENCES `doctor` (`Employees_Workers_wid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Administers_Nurse1` FOREIGN KEY (`Nurse_Employees_Workers_wid`) REFERENCES `nurse` (`Employees_Workers_wid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Administers_Technician1` FOREIGN KEY (`Technician_Employees_Workers_wid`) REFERENCES `technician` (`Employees_Workers_wid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Administers_Treatments1` FOREIGN KEY (`Treatments_idTreatment`) REFERENCES `treatments` (`idTreatment`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administers`
--

LOCK TABLES `administers` WRITE;
/*!40000 ALTER TABLE `administers` DISABLE KEYS */;
INSERT INTO `administers` VALUES (1,NULL,5,NULL,1),(2,NULL,39,NULL,1),(3,NULL,NULL,38,1),(4,NULL,5,NULL,2),(5,NULL,NULL,18,2),(6,NULL,5,NULL,3),(7,NULL,5,NULL,4),(8,NULL,5,NULL,5),(9,NULL,5,NULL,6),(10,40,NULL,NULL,6),(11,NULL,5,NULL,7),(12,NULL,5,NULL,8),(13,NULL,NULL,18,8),(14,NULL,NULL,38,8),(15,NULL,5,NULL,9),(16,NULL,NULL,1,9),(17,NULL,5,NULL,10),(18,7,NULL,NULL,10),(19,NULL,5,NULL,11),(20,NULL,5,NULL,12),(21,NULL,19,NULL,12),(22,NULL,5,NULL,13),(23,8,NULL,NULL,13),(24,9,NULL,NULL,13),(25,NULL,5,NULL,14),(26,NULL,NULL,16,14),(27,NULL,NULL,17,14),(28,NULL,5,NULL,15),(29,NULL,NULL,18,15),(30,NULL,NULL,1,15),(31,NULL,5,NULL,16),(32,7,NULL,NULL,16),(33,NULL,5,NULL,17),(34,22,NULL,NULL,17),(35,8,NULL,NULL,17),(36,NULL,5,NULL,18),(37,NULL,5,NULL,19),(38,9,NULL,NULL,19),(39,22,NULL,NULL,19),(40,NULL,5,NULL,20),(41,23,NULL,NULL,20),(42,NULL,NULL,1,20),(43,NULL,5,NULL,21),(44,24,NULL,NULL,21),(45,NULL,5,NULL,22),(46,NULL,5,NULL,23),(47,40,NULL,NULL,23),(48,NULL,NULL,2,23),(49,NULL,5,NULL,24),(50,NULL,5,NULL,25),(51,NULL,5,NULL,26),(52,NULL,5,NULL,27),(53,NULL,5,NULL,28),(54,NULL,NULL,3,28),(55,NULL,NULL,16,28),(56,NULL,5,NULL,29),(57,NULL,NULL,17,29),(58,NULL,5,NULL,30),(59,NULL,NULL,18,30),(60,NULL,NULL,38,30),(61,NULL,5,NULL,31),(62,NULL,39,NULL,31),(63,NULL,20,NULL,31),(64,NULL,5,NULL,32),(65,NULL,21,NULL,32),(66,NULL,20,NULL,32),(67,NULL,5,NULL,33),(68,NULL,5,NULL,34),(69,NULL,5,NULL,35),(70,NULL,5,NULL,36),(71,NULL,5,NULL,37),(72,NULL,5,NULL,38),(73,NULL,5,NULL,39),(74,NULL,5,NULL,40),(75,NULL,5,NULL,41),(76,NULL,5,NULL,42),(77,NULL,19,NULL,42),(78,NULL,5,NULL,43),(79,NULL,4,NULL,43),(80,NULL,5,NULL,44),(81,NULL,4,NULL,44),(82,NULL,19,NULL,44),(83,NULL,5,NULL,45),(84,NULL,19,NULL,45),(85,NULL,NULL,16,45),(86,NULL,5,NULL,46),(87,NULL,5,NULL,47),(88,NULL,5,NULL,48),(89,NULL,5,NULL,49),(90,NULL,5,NULL,50),(91,NULL,5,NULL,51),(92,NULL,5,NULL,52),(93,NULL,5,NULL,53),(94,NULL,5,NULL,54),(95,NULL,5,NULL,55),(96,NULL,5,NULL,56),(97,NULL,NULL,17,56),(98,8,NULL,NULL,56),(99,NULL,5,NULL,57),(100,7,NULL,NULL,57),(101,NULL,NULL,18,57),(102,NULL,5,NULL,58),(103,NULL,5,NULL,59),(104,NULL,NULL,38,59),(105,24,NULL,NULL,59),(106,NULL,5,NULL,60),(107,NULL,4,NULL,60),(108,NULL,NULL,3,60),(109,NULL,5,NULL,61),(110,NULL,5,NULL,62),(111,NULL,5,NULL,63),(112,NULL,5,NULL,64),(113,NULL,NULL,17,64),(114,NULL,5,NULL,65),(115,40,NULL,NULL,65),(116,NULL,5,NULL,66),(117,23,NULL,NULL,66),(118,NULL,5,NULL,67),(119,NULL,5,NULL,68),(120,NULL,20,NULL,68),(121,NULL,21,NULL,68),(122,NULL,5,NULL,69),(123,NULL,5,NULL,70),(124,NULL,5,NULL,71),(125,NULL,NULL,18,71),(126,NULL,5,NULL,72),(127,NULL,5,NULL,73),(128,NULL,NULL,38,73),(129,NULL,4,NULL,73),(130,NULL,5,NULL,74),(131,NULL,NULL,16,74),(132,NULL,5,NULL,75),(133,NULL,NULL,17,75),(134,NULL,NULL,18,75),(135,NULL,5,NULL,76),(136,40,NULL,NULL,76),(137,NULL,NULL,16,76),(138,NULL,5,NULL,77),(139,NULL,NULL,1,77),(140,NULL,5,NULL,78),(141,NULL,NULL,2,78),(142,NULL,5,NULL,79),(143,NULL,19,NULL,79),(144,NULL,5,NULL,80),(145,22,NULL,NULL,80),(146,NULL,NULL,1,80);
/*!40000 ALTER TABLE `administers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `administration`
--

DROP TABLE IF EXISTS `administration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administration` (
  `Employees_Workers_wid` int(11) NOT NULL,
  PRIMARY KEY (`Employees_Workers_wid`),
  CONSTRAINT `fk_Administration_Employees1` FOREIGN KEY (`Employees_Workers_wid`) REFERENCES `employees` (`Workers_wid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administration`
--

LOCK TABLES `administration` WRITE;
/*!40000 ALTER TABLE `administration` DISABLE KEYS */;
INSERT INTO `administration` VALUES (10),(11),(12),(25),(26),(27);
/*!40000 ALTER TABLE `administration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diagnosis`
--

DROP TABLE IF EXISTS `diagnosis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diagnosis` (
  `idDiagnosis` int(11) NOT NULL AUTO_INCREMENT,
  `diagnosis_name` varchar(45) NOT NULL,
  `Doctor_Employees_Workers_wid` int(11) NOT NULL,
  `diagnosis_nameid` int(11) DEFAULT NULL,
  PRIMARY KEY (`idDiagnosis`),
  KEY `fk_Diagnosis_Doctor1_idx` (`Doctor_Employees_Workers_wid`),
  CONSTRAINT `fk_Diagnosis_Doctor1` FOREIGN KEY (`Doctor_Employees_Workers_wid`) REFERENCES `doctor` (`Employees_Workers_wid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diagnosis`
--

LOCK TABLES `diagnosis` WRITE;
/*!40000 ALTER TABLE `diagnosis` DISABLE KEYS */;
INSERT INTO `diagnosis` VALUES (1,'Congestive Heart Failure',1,12),(2,'Cold',1,11),(3,'Flu',1,18),(4,'Meningitis',1,25),(5,'Flu',1,18),(6,'STD',1,31),(7,'Uniary Tract Infection',1,33),(8,'Diabetes',1,15),(9,'Broken Arm',1,4),(10,'Arthritis',1,1),(11,'Stroke',1,32),(12,'Dementia',1,13),(13,'Dementia',2,13),(14,'Asthma',1,2),(15,'Emphysema',2,16),(16,'STD',1,31),(17,'Depression',3,14),(18,'Broken Leg',3,5),(19,'Cancer',3,7),(20,'Cancer',3,7),(21,'Chronic Pain',3,10),(22,'Congestive Heart Failure',1,12),(23,'STD',3,31),(24,'Cancer',2,7),(25,'Stroke',3,32),(26,'Fibromyalgia',2,17),(27,'Diabetes',3,15),(28,'Heart Disease',3,19),(29,'Hypertention',3,21),(30,'STD',2,31),(31,'Uniary Tract Infection',2,33),(32,'Uniary Tract Infection',2,33),(33,'Chronic Pain',2,10),(34,'Cancer',1,7),(35,'Broken Pelvis',3,6),(36,'HIV',1,20),(37,'Stroke',2,32),(38,'Spina Bifida',3,30),(39,'Asthma',2,2),(40,'Pneumonia',1,29),(41,'Pneumonia',3,29),(42,'Pneumonia',1,29),(43,'Pneumonia',3,29),(44,'Osteoporosis',1,27),(45,'Liver Failure',2,23),(46,'Meningitis',3,25),(47,'Uniary Tract Infection',1,33),(48,'Asthma',3,2),(49,'Stroke',3,32),(50,'Lupus',2,24),(51,'Liver Failure',2,23),(52,'Kidney Failure',1,22),(53,'Uniary Tract Infection',3,33),(54,'Pneumonia',1,29),(55,'Asthma',2,2),(56,'Flu',3,18),(57,'Cold',2,11),(58,'STD',3,31),(59,'STD',1,31),(60,'Cold',2,11),(61,'Flu',3,18),(62,'Flu',2,18),(63,'Stroke',1,32),(64,'Congestive Heart Failure',3,12),(65,'Asthma',1,2),(66,'Cancer',3,7),(67,'Cancer',2,7),(68,'STD',3,31),(69,'Uniary Tract Infection',1,33),(70,'Cold',2,11),(71,'Multiple Sclerosis',1,26),(72,'Osteoporosis',3,27),(73,'Parkinson\'s Disease',1,28),(74,'Cold',2,11),(75,'Cancer',3,7),(76,'Flu',1,18),(77,'Spina Bifida',3,30),(78,'Pneumonia',2,29),(79,'Flu',1,18),(80,'Chronic Pain',3,10),(81,'Cancer',2,7),(82,'Cholesterol',1,8),(83,'Cholesterol',3,8),(84,'Autism',1,3),(85,'Cancer',3,7);
/*!40000 ALTER TABLE `diagnosis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctor` (
  `Employees_Workers_wid` int(11) NOT NULL,
  `admitting_privileges` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Employees_Workers_wid`),
  CONSTRAINT `fk_Doctor_Employees1` FOREIGN KEY (`Employees_Workers_wid`) REFERENCES `employees` (`Workers_wid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor`
--

LOCK TABLES `doctor` WRITE;
/*!40000 ALTER TABLE `doctor` DISABLE KEYS */;
INSERT INTO `doctor` VALUES (1,1),(2,1),(3,1),(16,0),(17,0),(18,0),(38,0);
/*!40000 ALTER TABLE `doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctor_admits_inpatient`
--

DROP TABLE IF EXISTS `doctor_admits_inpatient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctor_admits_inpatient` (
  `Doctor_Employees_Workers_wid` int(11) NOT NULL,
  `InPatient_InPatient` int(11) NOT NULL,
  `insurance` varchar(45) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `emergency_contact` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Doctor_Employees_Workers_wid`,`InPatient_InPatient`),
  KEY `fk_Doctor_has_InPatient_InPatient1_idx` (`InPatient_InPatient`),
  KEY `fk_Doctor_has_InPatient_Doctor1_idx` (`Doctor_Employees_Workers_wid`),
  CONSTRAINT `fk_Doctor_has_InPatient_Doctor1` FOREIGN KEY (`Doctor_Employees_Workers_wid`) REFERENCES `doctor` (`Employees_Workers_wid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Doctor_has_InPatient_InPatient1` FOREIGN KEY (`InPatient_InPatient`) REFERENCES `inpatient` (`InPatient`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor_admits_inpatient`
--

LOCK TABLES `doctor_admits_inpatient` WRITE;
/*!40000 ALTER TABLE `doctor_admits_inpatient` DISABLE KEYS */;
INSERT INTO `doctor_admits_inpatient` VALUES (1,1,'X12312312312','2014-06-07','241-213-2222'),(1,2,'J09090909090','2013-06-01','900-900-9009'),(1,3,'R77777777777','2012-08-10','334-123-0989'),(1,8,'H77788899901','1990-11-01','239-082-4589'),(1,9,'Q22449900881','2013-03-01','888-888-9999'),(1,10,'B33499099081','1986-09-09','234-234-5689'),(1,17,'E33344488891','2013-10-10','334-090-9900'),(1,20,'E45690123456','2013-05-01','334-555-0987'),(2,4,'K89189189189','2001-02-14','343-090-3430'),(2,5,'T88899900011','2012-08-11','980-444-5689'),(2,11,'G84930920109','1986-10-09','880-090-0999'),(2,12,'Y99989998999','1991-02-20','231-678-7859'),(2,13,'G66866866866','1992-03-09','445-090-7782'),(2,18,'N78978978978','2001-09-10','334-334-3344'),(3,6,'Q33344400099','1985-02-12','210-345-0945'),(3,7,'M23223223212','1990-10-03','456-098-3456'),(3,14,'X45559990001','2014-04-01','345-889-6578'),(3,15,'R22233399900','2013-11-09','290-909-7789'),(3,16,'Z33344455566','2000-09-11','445-909-4782'),(3,19,'U12309812309','2002-09-10','334-332-0909');
/*!40000 ALTER TABLE `doctor_admits_inpatient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `Workers_wid` int(11) NOT NULL,
  PRIMARY KEY (`Workers_wid`),
  CONSTRAINT `fk_Employees_Workers` FOREIGN KEY (`Workers_wid`) REFERENCES `workers` (`wid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21),(22),(23),(24),(25),(26),(27),(28),(29),(30),(37),(38),(39),(40);
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inpatient`
--

DROP TABLE IF EXISTS `inpatient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inpatient` (
  `InPatient` int(11) NOT NULL AUTO_INCREMENT,
  `Patient_idPatient` int(11) NOT NULL,
  `Diagnosis_idDiagnosis` int(11) NOT NULL,
  PRIMARY KEY (`InPatient`),
  KEY `fk_InPatient_Patient1_idx` (`Patient_idPatient`),
  KEY `fk_InPatient_Diagnosis1_idx` (`Diagnosis_idDiagnosis`),
  CONSTRAINT `fk_InPatient_Diagnosis1` FOREIGN KEY (`Diagnosis_idDiagnosis`) REFERENCES `diagnosis` (`idDiagnosis`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_InPatient_Patient1` FOREIGN KEY (`Patient_idPatient`) REFERENCES `patient` (`idPatient`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inpatient`
--

LOCK TABLES `inpatient` WRITE;
/*!40000 ALTER TABLE `inpatient` DISABLE KEYS */;
INSERT INTO `inpatient` VALUES (1,1,1),(2,2,4),(3,1,7),(4,2,8),(5,1,9),(6,2,10),(7,3,11),(8,4,12),(9,5,13),(10,1,14),(11,2,15),(12,4,19),(13,5,20),(14,6,21),(15,7,22),(16,8,24),(17,9,25),(18,10,26),(19,11,28),(20,12,29),(21,6,30),(22,7,34),(23,8,35),(24,9,36),(25,13,37),(26,14,38),(27,15,40),(28,16,41),(29,17,42),(30,1,43),(31,2,44),(32,3,50),(33,4,51),(34,6,52),(35,9,54),(36,18,63),(37,19,64),(38,12,66),(39,13,67),(40,18,68),(41,7,71),(42,19,72),(43,20,73),(44,19,75),(45,2,77),(46,3,78),(47,6,80),(48,7,81),(49,9,84),(50,11,85);
/*!40000 ALTER TABLE `inpatient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inpatient_has_treatments`
--

DROP TABLE IF EXISTS `inpatient_has_treatments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inpatient_has_treatments` (
  `InPatient_InPatient` int(11) NOT NULL,
  `Treatments_idTreatment` int(11) NOT NULL,
  PRIMARY KEY (`InPatient_InPatient`,`Treatments_idTreatment`),
  KEY `fk_InPatient_has_Treatments_Treatments1_idx` (`Treatments_idTreatment`),
  KEY `fk_InPatient_has_Treatments_InPatient1_idx` (`InPatient_InPatient`),
  CONSTRAINT `fk_InPatient_has_Treatments_InPatient1` FOREIGN KEY (`InPatient_InPatient`) REFERENCES `inpatient` (`InPatient`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_InPatient_has_Treatments_Treatments1` FOREIGN KEY (`Treatments_idTreatment`) REFERENCES `treatments` (`idTreatment`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inpatient_has_treatments`
--

LOCK TABLES `inpatient_has_treatments` WRITE;
/*!40000 ALTER TABLE `inpatient_has_treatments` DISABLE KEYS */;
INSERT INTO `inpatient_has_treatments` VALUES (1,1),(2,2),(3,6),(4,8),(5,9),(6,10),(7,12),(8,13),(9,14),(10,15),(11,16),(12,17),(13,19),(14,20),(15,21),(16,23),(17,28),(18,29),(19,30),(20,31),(21,32),(22,33),(23,34),(24,35),(25,42),(26,43),(27,44),(28,45),(29,56),(30,57),(31,58),(32,59),(33,60),(34,61),(35,62),(36,64),(37,65),(38,66),(39,67),(40,68),(41,71),(42,72),(43,73),(44,74),(45,75),(46,76),(47,77),(48,78),(49,79),(50,80);
/*!40000 ALTER TABLE `inpatient_has_treatments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nurse`
--

DROP TABLE IF EXISTS `nurse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nurse` (
  `Employees_Workers_wid` int(11) NOT NULL,
  PRIMARY KEY (`Employees_Workers_wid`),
  CONSTRAINT `fk_Nurse_Employees1` FOREIGN KEY (`Employees_Workers_wid`) REFERENCES `employees` (`Workers_wid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nurse`
--

LOCK TABLES `nurse` WRITE;
/*!40000 ALTER TABLE `nurse` DISABLE KEYS */;
INSERT INTO `nurse` VALUES (2),(4),(5),(19),(20),(21),(39);
/*!40000 ALTER TABLE `nurse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outpatient`
--

DROP TABLE IF EXISTS `outpatient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outpatient` (
  `OutPatient` int(11) NOT NULL AUTO_INCREMENT,
  `Patient_idPatient` int(11) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `Diagnosis_idDiagnosis` int(11) NOT NULL,
  PRIMARY KEY (`OutPatient`),
  KEY `fk_OutPatient_Patient1_idx` (`Patient_idPatient`),
  KEY `fk_OutPatient_Diagnosis1_idx` (`Diagnosis_idDiagnosis`),
  CONSTRAINT `fk_OutPatient_Diagnosis1` FOREIGN KEY (`Diagnosis_idDiagnosis`) REFERENCES `diagnosis` (`idDiagnosis`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_OutPatient_Patient1` FOREIGN KEY (`Patient_idPatient`) REFERENCES `patient` (`idPatient`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outpatient`
--

LOCK TABLES `outpatient` WRITE;
/*!40000 ALTER TABLE `outpatient` DISABLE KEYS */;
INSERT INTO `outpatient` VALUES (1,2,'1981-01-13','1981-01-13',2),(2,2,'1981-01-14','1981-01-14',3),(3,1,'1981-02-09','1981-02-09',5),(4,3,'1981-02-09','1981-02-09',6),(5,2,'1984-01-02','1984-01-02',16),(6,2,'1984-01-03','1984-01-03',17),(7,5,'1984-01-04','1984-01-04',18),(8,1,'1985-02-12','1985-02-12',23),(9,12,'1985-02-19','1985-02-20',27),(10,11,'1988-10-10','1988-10-10',31),(11,7,'1988-10-11','1988-10-11',32),(12,11,'1988-10-12','1988-10-12',33),(13,10,'1991-02-19','1991-02-20',39),(14,6,'2000-09-11','2000-09-11',45),(15,4,'2000-09-12','2000-09-12',46),(16,14,'2000-09-13','2000-09-13',47),(17,6,'2000-09-14','2000-09-14',48),(18,4,'2000-09-15','2000-09-15',49),(19,1,'2001-01-13','2001-01-14',53),(20,6,'2001-09-10','2001-09-10',55),(21,3,'2001-09-10','2001-09-11',56),(22,14,'2001-09-10','2001-09-12',57),(23,1,'2001-09-10','2001-09-13',58),(24,14,'2002-09-10','2002-09-14',59),(25,17,'2003-09-10','2003-09-15',60),(26,1,'2003-09-10','2003-09-15',61),(27,1,'2004-09-10','2004-09-17',62),(28,1,'2005-09-10','2005-09-17',65),(29,1,'2012-08-10','2012-08-10',69),(30,9,'2012-08-10','2012-08-10',70),(31,2,'2013-04-01','2013-04-02',74),(32,6,'2013-05-01','2013-05-02',76),(33,6,'2013-10-10','2013-10-11',79),(34,15,'2014-04-01','2014-04-02',82),(35,20,'2014-07-19',NULL,83);
/*!40000 ALTER TABLE `outpatient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outpatient_has_treatments`
--

DROP TABLE IF EXISTS `outpatient_has_treatments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outpatient_has_treatments` (
  `OutPatient_OutPatient` int(11) NOT NULL,
  `Treatments_idTreatment` int(11) NOT NULL,
  PRIMARY KEY (`OutPatient_OutPatient`,`Treatments_idTreatment`),
  KEY `fk_OutPatient_has_Treatments_Treatments1_idx` (`Treatments_idTreatment`),
  KEY `fk_OutPatient_has_Treatments_OutPatient1_idx` (`OutPatient_OutPatient`),
  CONSTRAINT `fk_OutPatient_has_Treatments_OutPatient1` FOREIGN KEY (`OutPatient_OutPatient`) REFERENCES `outpatient` (`OutPatient`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_OutPatient_has_Treatments_Treatments1` FOREIGN KEY (`Treatments_idTreatment`) REFERENCES `treatments` (`idTreatment`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outpatient_has_treatments`
--

LOCK TABLES `outpatient_has_treatments` WRITE;
/*!40000 ALTER TABLE `outpatient_has_treatments` DISABLE KEYS */;
INSERT INTO `outpatient_has_treatments` VALUES (1,3),(2,4),(4,5),(5,7),(7,11),(8,18),(9,22),(10,24),(11,25),(12,26),(14,27),(15,36),(16,37),(17,38),(19,39),(20,40),(21,41),(22,46),(23,47),(24,48),(25,49),(26,50),(27,51),(28,52),(30,53),(31,54),(32,55),(33,63),(34,69),(35,70);
/*!40000 ALTER TABLE `outpatient_has_treatments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient` (
  `idPatient` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idPatient`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES (1,'Michael','Scott'),(2,'Homer','Simpson'),(3,'Tony','Soprano'),(4,'Hannibal','Lector'),(5,'Cosmo','Bradshaw'),(6,'Bridget','Jones'),(7,'Lara','Croft'),(8,'Rachel','Green'),(9,'Austin','Powers'),(10,'Vincent','Vega'),(11,'Forrest','Gump'),(12,'Dexter','Morgan'),(13,'Ron','Burgundy'),(14,'Ally','McBeal'),(15,'Jerry','Maguire'),(16,'Vivian','Ward'),(17,'Barney','Stinson'),(18,'Tracy','Morgan'),(19,'Mary','Jones'),(20,'Edward','Cullen'),(21,'Tyler','Durden'),(22,'Amanda','Woodward'),(23,'Amanda','Bynes'),(24,'Jen','Yu'),(25,'Tony','Stark'),(26,'Napoleon','Dynamite'),(27,'Niko','Bellic'),(28,'Walter','White'),(29,'Skyler','White'),(30,'Jesse','Pinkman'),(31,'Hank ','Schrader'),(32,'Gustavo','Fring'),(33,'Tyrion','Lannister'),(34,'Daenerys','Targaryen'),(35,'Lily','Aldrin'),(36,'Robin','Scherbatsky'),(37,'Ted','Mosby'),(38,'Marshall','Eriksen'),(39,'Arthur','Fonzarelli'),(40,'Lucy','Ricardo'),(41,'Omar','Little'),(42,'Lester','Freamon'),(43,'George','Jefferson'),(44,'Stewie','Griffin'),(45,'Lucy','Liu'),(46,'Kal','Penn'),(47,'Penelope','Cruz'),(48,'Stephen','Colbert'),(49,'Doogie','Howser'),(50,'Joaquin','Phoenix');
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `procedures`
--

DROP TABLE IF EXISTS `procedures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procedures` (
  `idprocedures` int(11) NOT NULL,
  `procedure_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idprocedures`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procedures`
--

LOCK TABLES `procedures` WRITE;
/*!40000 ALTER TABLE `procedures` DISABLE KEYS */;
/*!40000 ALTER TABLE `procedures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `procedures_and_medications`
--

DROP TABLE IF EXISTS `procedures_and_medications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procedures_and_medications` (
  `idprocedures_and_medications` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`idprocedures_and_medications`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procedures_and_medications`
--

LOCK TABLES `procedures_and_medications` WRITE;
/*!40000 ALTER TABLE `procedures_and_medications` DISABLE KEYS */;
INSERT INTO `procedures_and_medications` VALUES (1,'medication','Antibiotics'),(2,'medication','Insulin'),(3,'medication','tPA'),(4,'medication','Acetaminophen'),(5,'medication','Anti-inflamatory'),(6,'medication','Fish Oil'),(7,'medication','ibuprofen'),(8,'medication','Opioid'),(9,'medication','Anti-Depressant'),(10,'medication','Steroid'),(11,'medication','Corticosteroid'),(12,'procedure','Dialysis'),(13,'procedure','MRI'),(14,'procedure','Xray'),(15,'procedure','Bone realignment'),(16,'procedure','physical therapy'),(17,'procedure','Heart Surgery'),(18,'procedure','Chemotherapy'),(19,'procedure','radiation'),(20,'procedure','fluids');
/*!40000 ALTER TABLE `procedures_and_medications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room`
--

DROP TABLE IF EXISTS `room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room` (
  `idRoom` int(11) NOT NULL,
  PRIMARY KEY (`idRoom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room`
--

LOCK TABLES `room` WRITE;
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
INSERT INTO `room` VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20);
/*!40000 ALTER TABLE `room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_has_patient`
--

DROP TABLE IF EXISTS `room_has_patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room_has_patient` (
  `Room_idRoom` int(11) NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NULL DEFAULT NULL,
  `InPatient_InPatient` int(11) NOT NULL,
  PRIMARY KEY (`Room_idRoom`,`start_date`,`InPatient_InPatient`),
  KEY `fk_Room_has_Patient_Room1_idx` (`Room_idRoom`),
  KEY `fk_Room_has_Patient_InPatient1_idx` (`InPatient_InPatient`),
  CONSTRAINT `fk_Room_has_Patient_InPatient1` FOREIGN KEY (`InPatient_InPatient`) REFERENCES `inpatient` (`InPatient`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Room_has_Patient_Room1` FOREIGN KEY (`Room_idRoom`) REFERENCES `room` (`idRoom`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_has_patient`
--

LOCK TABLES `room_has_patient` WRITE;
/*!40000 ALTER TABLE `room_has_patient` DISABLE KEYS */;
INSERT INTO `room_has_patient` VALUES (1,'1981-01-01 06:00:00','1981-02-03 06:00:00',1),(1,'1981-02-09 06:00:00','1981-05-08 05:00:00',1),(1,'1982-09-01 05:00:00','1983-09-09 05:00:00',3),(1,'1984-01-02 06:00:00','1984-03-08 06:00:00',2),(1,'1985-02-19 06:00:00','1990-04-03 05:00:00',8),(1,'2014-07-20 06:10:58',NULL,8),(2,'1981-02-01 06:00:00','1981-03-09 05:00:00',2),(2,'1983-10-09 05:00:00','1993-09-09 05:00:00',3),(3,'1981-09-10 05:00:00','1981-09-12 05:00:00',2),(3,'1984-01-01 06:00:00','1984-02-09 06:00:00',1),(3,'1985-02-19 06:00:00','2005-09-01 05:00:00',9),(4,'1982-08-09 05:00:00','1982-10-09 05:00:00',1),(4,'1983-11-09 06:00:00','1995-09-09 05:00:00',4),(5,'1982-08-10 05:00:00','1983-02-09 06:00:00',2),(5,'1984-03-08 06:00:00','2003-09-08 05:00:00',5),(6,'1985-02-09 06:00:00','1985-05-19 05:00:00',1),(6,'1986-09-09 05:00:00','1987-10-10 05:00:00',10),(6,'1990-12-01 06:00:00','1990-12-19 06:00:00',1),(6,'1991-02-03 06:00:00','1991-02-04 06:00:00',1),(6,'1991-02-19 06:00:00','1991-02-21 06:00:00',1),(6,'1992-03-09 05:00:00','2013-04-09 05:00:00',13),(7,'1985-02-11 06:00:00','1985-04-29 05:00:00',2),(7,'1986-10-09 05:00:00','1986-10-10 05:00:00',11),(7,'1988-10-10 05:00:00','1989-12-01 06:00:00',1),(7,'1991-02-20 06:00:00','2003-09-09 05:00:00',12),(7,'2013-03-01 06:00:00','2013-09-18 05:00:00',9),(7,'2014-07-20 06:10:58',NULL,1),(8,'1985-02-12 06:00:00','1985-09-09 05:00:00',6),(8,'1990-10-03 05:00:00','2008-09-09 05:00:00',7),(8,'2000-09-09 05:00:00','2000-09-11 05:00:00',14),(8,'2001-01-09 06:00:00','2001-01-10 06:00:00',3),(8,'2001-01-11 06:00:00','2001-09-12 05:00:00',9),(8,'2001-02-14 06:00:00','2012-03-04 06:00:00',4),(8,'2013-04-01 05:00:00','2013-10-11 05:00:00',1),(9,'1985-02-19 06:00:00','1985-10-10 05:00:00',7),(9,'2000-09-09 05:00:00','2000-09-12 05:00:00',1),(9,'2001-01-13 06:00:00','2002-02-12 06:00:00',17),(9,'2001-09-10 05:00:00','2009-03-12 05:00:00',18),(10,'2000-09-10 05:00:00','2000-09-13 05:00:00',2),(10,'2002-09-10 05:00:00','2005-03-31 05:00:00',19),(10,'2013-05-01 05:00:00','2013-11-13 06:00:00',20),(11,'2000-09-11 05:00:00','2009-02-09 06:00:00',15),(11,'2013-06-01 05:00:00','2013-06-05 05:00:00',2),(12,'2000-09-11 05:00:00','2010-03-10 06:00:00',16),(12,'2013-10-10 05:00:00','2013-11-11 06:00:00',17),(13,'2012-08-09 05:00:00','2012-08-31 05:00:00',2),(13,'2013-11-09 06:00:00','2014-03-02 06:00:00',15),(14,'2014-07-20 06:10:58',NULL,3),(15,'2012-08-11 05:00:00','2013-01-01 06:00:00',5),(15,'2014-07-20 06:10:58',NULL,14);
/*!40000 ALTER TABLE `room_has_patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `idServices` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `Monday` tinyint(1) DEFAULT NULL,
  `Tuesday` tinyint(1) DEFAULT NULL,
  `Wednesday` tinyint(1) DEFAULT NULL,
  `Thursday` tinyint(1) DEFAULT NULL,
  `Friday` tinyint(1) DEFAULT NULL,
  `Saturday` tinyint(1) DEFAULT NULL,
  `Sunday` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idServices`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'gift shop',1,1,0,0,0,0,0),(2,'informational desk',0,0,1,1,0,0,0),(3,'snack carts',0,0,0,0,1,0,0),(4,'reading carts',1,1,1,1,0,0,0),(5,'snack carts',0,0,0,0,1,0,0),(6,'gift shop',1,1,0,0,0,0,0),(7,'informational desk',0,1,1,0,0,0,0),(8,'snack carts',0,0,1,1,0,0,0),(9,'reading carts',0,0,0,1,1,0,0),(10,'gift shop',0,1,1,0,0,0,0),(11,'informational desk',0,0,1,1,0,0,0),(12,'snack carts',0,0,0,1,1,0,0),(13,'reading carts',1,0,0,0,1,0,0),(14,'gift shop',1,1,0,0,0,0,0),(15,'informational desk',0,1,1,0,0,0,0),(16,'snack carts',0,0,1,1,0,0,0),(17,'gift shop',0,0,0,1,1,0,0),(18,'informational desk',1,0,0,0,1,0,0),(19,'snack carts',1,1,0,0,0,0,0),(20,'reading carts',0,1,1,0,0,0,0),(21,'cafeteria',1,1,1,1,1,1,0),(22,'cafeteria',1,1,1,1,0,1,1),(23,'gift shop',1,1,1,1,1,0,0),(24,'janitorial services',1,1,1,1,1,1,0),(25,'janitorial services',1,1,1,1,0,1,1);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_has_staff`
--

DROP TABLE IF EXISTS `services_has_staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_has_staff` (
  `Services_idServices` int(11) NOT NULL,
  `Staff_Employees_Workers_wid` int(11) NOT NULL,
  PRIMARY KEY (`Services_idServices`,`Staff_Employees_Workers_wid`),
  KEY `fk_Services_has_Staff_Staff1_idx` (`Staff_Employees_Workers_wid`),
  KEY `fk_Services_has_Staff_Services1_idx` (`Services_idServices`),
  CONSTRAINT `fk_Services_has_Staff_Services1` FOREIGN KEY (`Services_idServices`) REFERENCES `services` (`idServices`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Services_has_Staff_Staff1` FOREIGN KEY (`Staff_Employees_Workers_wid`) REFERENCES `staff` (`Employees_Workers_wid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_has_staff`
--

LOCK TABLES `services_has_staff` WRITE;
/*!40000 ALTER TABLE `services_has_staff` DISABLE KEYS */;
INSERT INTO `services_has_staff` VALUES (21,13),(22,14),(23,15),(24,28),(25,29);
/*!40000 ALTER TABLE `services_has_staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_has_volunteers`
--

DROP TABLE IF EXISTS `services_has_volunteers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_has_volunteers` (
  `Services_idServices` int(11) NOT NULL,
  `Volunteers_Workers_wid` int(11) NOT NULL,
  PRIMARY KEY (`Services_idServices`,`Volunteers_Workers_wid`),
  KEY `fk_Services_has_Volunteers_Volunteers1_idx` (`Volunteers_Workers_wid`),
  KEY `fk_Services_has_Volunteers_Services1_idx` (`Services_idServices`),
  CONSTRAINT `fk_Services_has_Volunteers_Services1` FOREIGN KEY (`Services_idServices`) REFERENCES `services` (`idServices`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Services_has_Volunteers_Volunteers1` FOREIGN KEY (`Volunteers_Workers_wid`) REFERENCES `volunteers` (`Workers_wid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_has_volunteers`
--

LOCK TABLES `services_has_volunteers` WRITE;
/*!40000 ALTER TABLE `services_has_volunteers` DISABLE KEYS */;
INSERT INTO `services_has_volunteers` VALUES (1,31),(2,31),(3,31),(4,32),(5,32),(6,33),(7,34),(8,35),(9,36),(10,41),(11,42),(12,43),(13,44),(14,45),(15,46),(16,47),(17,48),(18,49),(19,50),(20,50);
/*!40000 ALTER TABLE `services_has_volunteers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff` (
  `Employees_Workers_wid` int(11) NOT NULL,
  PRIMARY KEY (`Employees_Workers_wid`),
  CONSTRAINT `fk_Staff_Employees1` FOREIGN KEY (`Employees_Workers_wid`) REFERENCES `employees` (`Workers_wid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff`
--

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` VALUES (13),(14),(15),(28),(29),(30),(37);
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `technician`
--

DROP TABLE IF EXISTS `technician`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `technician` (
  `Employees_Workers_wid` int(11) NOT NULL,
  PRIMARY KEY (`Employees_Workers_wid`),
  CONSTRAINT `fk_Technician_Employees1` FOREIGN KEY (`Employees_Workers_wid`) REFERENCES `employees` (`Workers_wid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `technician`
--

LOCK TABLES `technician` WRITE;
/*!40000 ALTER TABLE `technician` DISABLE KEYS */;
INSERT INTO `technician` VALUES (7),(8),(9),(22),(23),(24),(40);
/*!40000 ALTER TABLE `technician` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `treatments`
--

DROP TABLE IF EXISTS `treatments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `treatments` (
  `idTreatment` int(11) NOT NULL AUTO_INCREMENT,
  `Doctor_Employees_Workers_wid` int(11) NOT NULL,
  `procedures_and_medications_idprocedures_and_medications` int(11) NOT NULL,
  PRIMARY KEY (`idTreatment`),
  KEY `fk_Treatments_Doctor1_idx` (`Doctor_Employees_Workers_wid`),
  KEY `fk_Treatments_procedures_and_medications1_idx` (`procedures_and_medications_idprocedures_and_medications`),
  CONSTRAINT `fk_Treatments_Doctor1` FOREIGN KEY (`Doctor_Employees_Workers_wid`) REFERENCES `doctor` (`Employees_Workers_wid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Treatments_procedures_and_medications1` FOREIGN KEY (`procedures_and_medications_idprocedures_and_medications`) REFERENCES `procedures_and_medications` (`idprocedures_and_medications`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `treatments`
--

LOCK TABLES `treatments` WRITE;
/*!40000 ALTER TABLE `treatments` DISABLE KEYS */;
INSERT INTO `treatments` VALUES (1,1,17),(2,2,1),(3,3,5),(4,1,1),(5,1,1),(6,1,2),(7,2,14),(8,3,7),(9,1,3),(10,1,11),(11,2,11),(12,3,1),(13,1,9),(14,2,15),(15,3,18),(16,1,19),(17,2,8),(18,3,17),(19,2,1),(20,2,18),(21,1,3),(22,2,8),(23,3,2),(24,1,6),(25,2,6),(26,3,1),(27,2,1),(28,2,1),(29,3,8),(30,2,19),(31,2,15),(32,1,1),(33,2,3),(34,3,11),(35,1,13),(36,2,13),(37,3,1),(38,2,1),(39,2,4),(40,2,1),(41,3,1),(42,3,11),(43,1,3),(44,2,5),(45,1,20),(46,2,12),(47,3,20),(48,2,20),(49,2,11),(50,2,20),(51,3,20),(52,2,1),(53,3,1),(54,2,20),(55,2,20),(56,2,20),(57,3,3),(58,3,17),(59,1,11),(60,2,18),(61,1,19),(62,2,1),(63,2,20),(64,2,20),(65,3,13),(66,3,10),(67,1,14),(68,2,8),(69,2,20),(70,3,2),(71,2,20),(72,3,10),(73,2,13),(74,2,20),(75,2,8),(76,3,18),(77,3,6),(78,1,6),(79,2,17),(80,1,18);
/*!40000 ALTER TABLE `treatments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `volunteers`
--

DROP TABLE IF EXISTS `volunteers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `volunteers` (
  `Workers_wid` int(11) NOT NULL,
  PRIMARY KEY (`Workers_wid`),
  CONSTRAINT `fk_Volunteers_Workers1` FOREIGN KEY (`Workers_wid`) REFERENCES `workers` (`wid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `volunteers`
--

LOCK TABLES `volunteers` WRITE;
/*!40000 ALTER TABLE `volunteers` DISABLE KEYS */;
INSERT INTO `volunteers` VALUES (31),(32),(33),(34),(35),(36),(41),(42),(43),(44),(45),(46),(47),(48),(49),(50);
/*!40000 ALTER TABLE `volunteers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workers`
--

DROP TABLE IF EXISTS `workers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workers` (
  `wid` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `date_of_hire` date DEFAULT NULL,
  PRIMARY KEY (`wid`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workers`
--

LOCK TABLES `workers` WRITE;
/*!40000 ALTER TABLE `workers` DISABLE KEYS */;
INSERT INTO `workers` VALUES (1,'Marilyn','Monroe','Doctor','1980-09-09'),(2,'Abraham','Lincoln','Doctor','1980-10-10'),(3,'John','Kennedy','Doctor','1980-11-10'),(4,'Martin','King','Nurse','1980-11-10'),(5,'Nelson','Mandela','Nurse','1980-11-10'),(6,'Winston','Churchhill','Nurse','1980-11-10'),(7,'Bill','Gates','Technician','1980-11-11'),(8,'Muhammand','Ali','Technician','1980-11-12'),(9,'Margaret','Thatcher','Technician','1980-11-13'),(10,'Charles','Gaulle','Administration','1980-11-14'),(11,'Christopher','Columbus','Administration','1980-11-15'),(12,'George','Orwell','Administration','1980-11-16'),(13,'Charles','Darwin','Staff','1980-11-17'),(14,'Elvis','Presley','Staff','1980-11-18'),(15,'Albert','Einstein','Staff','1980-11-19'),(16,'Jordan','Hullett','Doctor','1983-10-09'),(17,'Xiao','Li','Doctor','1983-11-01'),(18,'Kevin','Vasko','Doctor','1983-11-02'),(19,'Connie','McLaurin','Nurse','1983-11-03'),(20,'Louis','Pasteur','Nurse','1983-11-04'),(21,'Bin','Li','Nurse','1983-11-05'),(22,'Leo','Tolstoy','Technician','1986-12-01'),(23,'Vincent','Gogh','Technician','1990-01-01'),(24,'Thomas','Edison','Technician','1990-01-02'),(25,'Oprah','Winfrey','Administration','1990-01-03'),(26,'Peter','Sellers','Administration','1990-01-04'),(27,'Walt','Disney','Administration','1993-09-09'),(28,'Henry','Ford','Staff','1994-01-01'),(29,'Tiger','Woods','Staff','1995-08-09'),(30,'Bill','Nye','Staff','2001-03-01'),(31,'Gregory','House','Volunteer','2003-03-09'),(32,'Billy','Graham','Volunteer','2003-03-10'),(33,'Johnny','Depp','Volunteer','2007-09-10'),(34,'Lenardo','DiCaprio','Volunteer','2008-10-11'),(35,'Betty','White','Volunteer','2009-10-10'),(36,'Dean','Hendrix','Volunteer','2011-11-12'),(37,'Clark','Kent','Staff','2012-09-01'),(38,'James','Kirk','Doctor','2013-09-01'),(39,'Leonard','McCoy','Nurse','2014-02-19'),(40,'Candy','Cox','Technician','2014-05-09'),(41,'Lance','Armstrong','Volunteer','2014-06-08'),(42,'Scarlett','Johansson','Volunteer','2014-06-09'),(43,'Jimmy','Wales','Volunteer','2014-06-10'),(44,'Stephen','King','Volunteer','2014-06-11'),(45,'Bradan','Jalina','Volunteer','2014-06-12'),(46,'George','Clooney','Volunteer','2014-06-13'),(47,'Amy','Poehler','Volunteer','2014-06-14'),(48,'Kat','Williams','Volunteer','2014-06-15'),(49,'Dave','Chappelle','Volunteer','2014-06-16'),(50,'Chris','Rock','Volunteer','2014-06-17');
/*!40000 ALTER TABLE `workers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-07-20 15:07:01
