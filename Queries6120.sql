Queries can possibly show duplication if there is data integrity issues (e.g. the same patient is placed in two different rooms). We assume that the middleware is handling business logic issues such as this).

A1:

SELECT
    rhp.Room_IdRoom as 'Room',
    p.idPatient as 'Patient ID',
    p.fname as 'First Name',
    p.lname as 'Last Name',
    rhp.start_date as 'Admitted'
FROM
    inpatient as ip
        join
    room_has_patient as rhp ON ip.InPatient = rhp.InPatient_InPatient
        JOIN
    patient as p ON ip.Patient_idPatient = p.idPatient
WHERE
    rhp.end_date IS NULL;

A2:

(select 
    idRoom
from
    room
where
    room.idRoom NOT IN (select 
            Room_idRoom
        from
            room_has_patient as rhp
        where
            end_date is null)) UNION (select 
    idRoom
from
    room
where
    idRoom not in (select 
            Room_idRoom
        from
            room_has_patient));
A3:

SELECT 
    r.idroom,
    CASE
        when rhp.end_date IS null then rhp.start_date
        else null
    END as 'Admission Date',
    case
        when rhp.end_date is NULL then p.fname
        else NULL
    END as 'First Name',
    case
        when rhp.end_date is NULL then p.lname
        else NULL
    END as 'Last Name'
FROM
    room as r
        LEFT JOIN
    room_has_patient as rhp ON rhp.Room_idRoom = r.idRoom
        AND rhp.end_Date IS NULL
        LEFT JOIN
    inpatient as ip ON rhp.InPatient_InPatient = ip.InPatient
        LEFT JOIN
    patient as p ON ip.Patient_idPatient = p.idPatient;

B1:

SELECT 
    p.idPatient as 'Paitent ID',
    p.fname as 'First Name',
    p.lname as 'Last Name'
FROM
    patient as p;

B.2: 

SELECT Distinct
    p.idPatient as 'Patient Identification',
    p.fname 'First Name',
    p.lname as 'Last Name'
FROM
    inpatient as ip
        JOIN
    room_has_patient as rhp ON rhp.InPatient_InPatient = ip.InPatient
        JOIN
    patient as p ON ip.Patient_idPatient = p.idPatient
WHERE
    rhp.end_date IS NULL;

B.3: (change start_date and CURRENT_TIMESTAMP as needed)

SELECT distinct
    p.idPatient as 'Patient Identification',
    p.fname 'First Name',
    p.lname as 'Last Name'
FROM
    inpatient as ip
        JOIN
    room_has_patient as rhp ON rhp.InPatient_InPatient = ip.InPatient
        JOIN
    patient as p ON ip.Patient_idPatient = p.idPatient
WHERE
    rhp.start_date BETWEEN '2012-01-01' AND CURRENT_TIMESTAMP
        AND (rhp.end_date BETWEEN '2012-01-01' AND CURRENT_TIMESTAMP
        OR rhp.end_date IS NULL);

B.4: 

SELECT DISTINCT
    p.idPatient as 'idPatient',
    p.fname as 'First Name',
    p.lname as 'Last Name'
FROM
    inpatient as ip
        JOIN
    room_has_patient as rhp ON ip.InPatient = rhp.InPatient_InPatient
        JOIN
    patient as p ON p.idPatient = ip.Patient_idPatient
WHERE
    end_date BETWEEN '2000-01-01' AND CURRENT_TIMESTAMP;


B.5:

SELECT 
    p.idPatient as 'idPatient',
    p.fname as 'First Name',
    p.lname as 'Last Name'
FROM
    outpatient as op
        JOIN
    patient as p ON op.Patient_idPatient = p.idPatient
WHERE
    end_date IS NULL;

B.6:

SELECT distinct
    p.idPatient as 'idPatient',
    p.fname as 'First Name',
    p.lname as 'Last Name'
FROM
    outpatient as op
        JOIN
    patient as p ON op.Patient_idPatient = p.idPatient
WHERE
    end_date BETWEEN '2001-01-01' AND CURRENT_TIMESTAMP;

B.7: 

SELECT 
    *
FROM
    inpatient as ip
        JOIN
    patient as p ON ip.Patient_idPatient = p.idPatient
        JOIN
    diagnosis as d ON d.idDiagnosis = ip.Diagnosis_idDiagnosis
WHERE
    p.idPatient = 2
        OR (p.fname = 'Homer'
        AND p.lname = 'Simpson');

B.8: 

SELECT 
    ip.InPatient, pam.name
FROM
    patient as p
        JOIN
    inpatient as ip ON p.idPatient = ip.Patient_idPatient
        JOIN
    inpatient_has_treatments as iht ON ip.InPatient = iht.InPatient_InPatient
        JOIN
    treatments as t ON iht.Treatments_idTreatment = t.idTreatment
        JOIN
    procedures_and_medications as pam ON t.procedures_and_medications_idprocedures_and_medications = pam.idprocedures_and_medications
WHERE
    p.idPatient = 18
        OR (p.fname = 'Tracy' AND p.lname = 'Morgan')
GROUP BY ip.InPatient , pam.name
ORDER BY ip.InPatient DESC , t.idTreatment;

B.9 (we are assuming query can return duplicates). 

SELECT 
    p.idPatient as 'Patient ID',
    p.fname as 'Patient First Name',
    p.lname as 'Patient Last Name',
    d.diagnosis_name as 'Diagnosis Name',
    d.Doctor_Employees_Workers_wid as 'Admitting Dr. ID'
FROM
    inpatient as ip
        JOIN
    room_has_patient as rhp ON ip.InPatient = rhp.InPatient_InPatient
        JOIN
    (SELECT 
        *
    FROM
        inpatient as ip
    JOIN room_has_patient as rhp ON ip.InPatient = rhp.InPatient_InPatient) as opp_row ON ip.InPatient = opp_row.InPatient
        AND DATEDIFF(rhp.start_date, opp_row.end_date) BETWEEN 0 AND 30
        JOIN
    patient as p ON ip.Patient_idPatient = p.idPatient
        JOIN
    diagnosis as d ON ip.Diagnosis_idDiagnosis = idDiagnosis;

B10:

SELECT 
    ip.Patient_idPatient,
    (SELECT 
            count(*)
        FROM
            room_has_patient count_rhp
                JOIN
            inpatient as count_ip ON count_rhp.InPatient_InPatient = count_ip.InPatient
        WHERE
            count_ip.Patient_idPatient = ip.Patient_idPatient
        GROUP BY count_ip.Patient_idPatient) as 'Admission Count',
    (SELECT 
            avg(DATEDIFF(avg_rhp.end_date, avg_rhp.start_date))
        FROM
            comp6120_project.room_has_patient avg_rhp
                JOIN
            inpatient as avg_ip ON avg_rhp.InPatient_InPatient = avg_ip.InPatient
        WHERE
            avg_ip.Patient_idPatient = ip.Patient_idPatient
        GROUP BY avg_ip.Patient_idPatient) as 'Average Stay Length',
    (SELECT 
            min(DATEDIFF(min_rhp.end_date, min_rhp.start_date))
        FROM
            room_has_patient min_rhp
                JOIN
            inpatient as min_ip ON min_rhp.InPatient_InPatient = min_ip.InPatient
        WHERE
            min_ip.Patient_idPatient = ip.Patient_idPatient
        GROUP BY min_ip.Patient_idPatient) as 'Minimum Stay Length',
    (SELECT 
            max(DATEDIFF(max_rhp.end_date, max_rhp.start_date))
        FROM
            room_has_patient max_rhp
                JOIN
            inpatient as max_ip ON max_rhp.InPatient_InPatient = max_ip.InPatient
        WHERE
            max_ip.Patient_idPatient = ip.Patient_idPatient
        GROUP BY max_ip.Patient_idPatient) as 'Maximum Stay Length'
FROM
    inpatient as ip
        JOIN
    room_has_patient as rhp ON rhp.inpatient_inpatient = ip.InPatient
GROUP BY patient_idPatient;

C1:

SELECT 
    d.diagnosis_nameid as 'Diagnosis ID',
    d.diagnosis_name as 'Diagnosis Name',
    count(*) as 'Occurances'
FROM
    diagnosis as d
        JOIN
    inpatient as ip ON d.idDiagnosis = ip.Diagnosis_idDiagnosis
GROUP BY d.diagnosis_nameid , d.diagnosis_name
ORDER BY count(*) DESC;

C2:

SELECT 
    d.diagnosis_nameid as 'Diagnosis ID',
    d.diagnosis_name as 'Diagnosis Name',
    count(*) as 'Occurances'
FROM
    diagnosis as d
        JOIN
    outpatient as op ON d.idDiagnosis = op.Diagnosis_idDiagnosis
GROUP BY d.diagnosis_nameid , d.diagnosis_name
ORDER BY count(*) DESC;

C3:

SELECT 
    d.diagnosis_nameid as 'Diagnosis ID',
    d.diagnosis_name as 'Diagnosis Name',
    count(*) as Occurances
FROM
    diagnosis as d
        JOIN
    inpatient as ip ON d.idDiagnosis = ip.Diagnosis_idDiagnosis
GROUP BY d.diagnosis_nameid , d.diagnosis_name 
UNION ALL SELECT 
    d.diagnosis_nameid as 'Diagnosis ID',
    d.diagnosis_name as 'Diagnosis Name',
    count(*) as 'Occurances'
FROM
    diagnosis as d
        JOIN
    outpatient as op ON d.idDiagnosis = op.Diagnosis_idDiagnosis
GROUP BY d.diagnosis_nameid , d.diagnosis_name
ORDER BY Occurances DESC;

C4: 

SELECT 
    idprocedures_and_medications, name, count(*) as 'Occurances'
FROM
    (SELECT 
        *
    FROM
        (SELECT 
        pam.idprocedures_and_medications, pam.name
    FROM
        inpatient as ip
    JOIN inpatient_has_treatments iht ON ip.InPatient = iht.InPatient_InPatient
    JOIN treatments as t ON iht.treatments_idTreatment = t.idTreatment
    JOIN procedures_and_medications as pam ON t.procedures_and_medications_idprocedures_and_medications = pam.idprocedures_and_medications) as ipatient UNION ALL SELECT 
        *
    FROM
        (SELECT 
        pam.idprocedures_and_medications, pam.name
    FROM
        outpatient as op
    JOIN outpatient_has_treatments oht ON op.OutPatient = oht.OutPatient_OutPatient
    JOIN treatments as t ON oht.treatments_idTreatment = t.idTreatment
    JOIN procedures_and_medications as pam ON t.procedures_and_medications_idprocedures_and_medications = pam.idprocedures_and_medications) as opatient) as answer
GROUP BY idprocedures_and_medications , name
ORDER BY Occurances DESC;

C5:

SELECT 
    *
FROM
    (SELECT 
        pam.idprocedures_and_medications,
            pam.name,
            count(*) as 'Occurances'
    FROM
        inpatient as ip
    JOIN inpatient_has_treatments iht ON ip.InPatient = iht.InPatient_InPatient
    JOIN treatments as t ON iht.treatments_idTreatment = t.idTreatment
    JOIN procedures_and_medications as pam ON t.procedures_and_medications_idprocedures_and_medications = pam.idprocedures_and_medications
    GROUP BY pam.idprocedures_and_medications , pam.name) as ipatient
ORDER BY Occurances DESC;

C6:

SELECT 
    *
FROM
    (SELECT 
        pam.idprocedures_and_medications,
            pam.name,
            count(*) as 'Occurances'
    FROM
        outpatient as op
    JOIN outpatient_has_treatments oht ON op.OutPatient = oht.OutPatient_OutPatient
    JOIN treatments as t ON oht.treatments_idTreatment = t.idTreatment
    JOIN procedures_and_medications as pam ON t.procedures_and_medications_idprocedures_and_medications = pam.idprocedures_and_medications
    GROUP BY pam.idprocedures_and_medications , pam.name) as opatient
ORDER BY Occurances DESC;

C7:

select 
    diagnosis_name, count(*)
from
    (select 
        inPatient.Patient_idPatient as pid,
            Diagnosis_idDiagnosis,
            diagnosis.diagnosis_name
    from
        diagnosis
    join inpatient ON diagnosis.idDiagnosis = inpatient.Diagnosis_idDiagnosis union select 
        outpatient.Patient_idPatient as pid,
            Diagnosis_idDiagnosis,
            diagnosis.diagnosis_name
    from
        diagnosis
    join outpatient ON diagnosis.idDiagnosis = outpatient.Diagnosis_idDiagnosis) as se
where
    pid in (select 
            Patient_idPatient
        from
            patient
                join
            inpatient ON patient.idPatient = inpatient.Patient_idPatient
                join
            diagnosis ON diagnosis.idDiagnosis = inpatient.Diagnosis_idDiagnosis
        group by inpatient.Patient_idPatient
        having count(patient_idpatient) = (select 
                max(cnt.patid)
            from
                (select 
                    count(patient_idPatient) as patid
                from
                    inpatient
                group by Patient_idPatient) as cnt))
group by diagnosis_name
order by count(*) asc;

C8: (idTreatment can be anything please change accordingly)

SELECT 
    *
FROM
    (SELECT 
        a.technician_employees_Workers_wid as 'Administer Technician ID',
            a.Nurse_Employees_workers_wid as 'Administer Nurse ID',
            a.Doctor_employees_Workers_wid as 'Administer Doctor ID',
            t.Doctor_Employees_Workers_wid as 'Treatment Ordering Dr.',
            p.fname 'Patient First Name',
            p.lname 'Patient Last Name',
            t.idTreatment
    FROM
        treatments as t
    JOIN procedures_and_medications as pam ON t.procedures_and_medications_idprocedures_and_medications = idprocedures_and_medications
    JOIN administers as a ON t.idTreatment = a.Treatments_idTreatment
    JOIN inpatient_has_treatments as iht ON t.idTreatment = iht.Treatments_idTreatment
    JOIN InPatient as ip ON iht.InPatient_InPatient = ip.InPatient
    JOIN Patient as p ON ip.Patient_idPatient = p.idPatient UNION SELECT 
        a.technician_employees_Workers_wid as 'Administer Technician ID',
            a.Nurse_Employees_workers_wid as 'Administer Nurse ID',
            a.Doctor_employees_Workers_wid as 'Administer Doctor ID',
            t.Doctor_Employees_Workers_wid as 'Treatment Ordering Dr.',
            p.fname 'Patient First Name',
            p.lname 'Patient Last Name',
            t.idTreatment
    FROM
        treatments as t
    JOIN procedures_and_medications as pam ON t.procedures_and_medications_idprocedures_and_medications = idprocedures_and_medications
    JOIN administers as a ON t.idTreatment = a.Treatments_idTreatment
    JOIN outpatient_has_treatments as oht ON t.idTreatment = oht.Treatments_idTreatment
    JOIN OutPatient as op ON oht.OutPatient_OutPatient = op.OutPatient
    JOIN Patient as p ON op.Patient_idPatient = p.idPatient) as ans
WHERE
    idTreatment = 2;

D1:

select 
    wks.wid, wks.lname, wks.fname, wks.date_of_hire, wks.type
from
    workers as wks
order by lname asc , fname asc;

D2:

select distinct
    lname, fname
from
    workers,
    volunteers,
    services_has_volunteers,
    services
where
    workers.wid = volunteers.Workers_wid
        AND volunteers.Workers_wid = services_has_volunteers.Volunteers_Workers_wid
        AND services_has_volunteers.Services_idServices = services.idServices
        AND services.Tuesday = 1
        AND services.type = 'informational desk';

D3:

select distinct
    workers.fname, workers.lname
from
    workers
        join
    employees ON workers.wid = employees.Workers_wid
        join
    doctor ON doctor.Employees_Workers_wid = employees.Workers_wid
        join
    (SELECT 
        daip1.Doctor_Employees_Workers_wid
    FROM
        doctor_admits_inpatient as daip1
    JOIN doctor_admits_inpatient as daip2 ON daip1.Doctor_Employees_Workers_wid = daip2.Doctor_Employees_Workers_wid
        AND DATEDIFF(daip1.date, daip2.date) < 365
        AND DATEDIFF(daip1.date, daip2.date) > 0
    GROUP BY daip1.Doctor_Employees_Workers_wid
    HAVING count(daip1.Doctor_Employees_Workers_wid) > 4) as daip3 ON daip3.Doctor_Employees_Workers_wid = doctor.Employees_Workers_wid
        JOIN
    diagnosis ON daip3.Doctor_Employees_Workers_wid = diagnosis.Doctor_Employees_Workers_wid

D.4: (worker name can be anyone change accordingly)

select 
    diagnosis_name, count(diagnosis.diagnosis_name)
from
    doctor
        join
    diagnosis ON doctor.Employees_Workers_wid = diagnosis.Doctor_Employees_Workers_wid
        join
    employees ON employees.Workers_wid = doctor.Employees_Workers_wid
        join
    workers ON workers.wid = employees.Workers_wid
where
    workers.lname = 'monroe'
        AND workers.fname = 'marilyn'
group by diagnosis_name
order by count(diagnosis.diagnosis_name) desc;

D.5:

SELECT 
    pam.name, count(*)
FROM
    Doctor as d
        JOIN
    Employees as e ON d.employees_Workers_wid = Workers_wid
        JOIN
    Workers as w ON e.Workers_wid = w.wid
        JOIN
    treatments as t ON d.employees_Workers_wid = t.Doctor_Employees_Workers_wid
        JOIN
    procedures_and_medications as pam ON t.procedures_and_medications_idprocedures_and_medications = pam.idprocedures_and_medications
WHERE
    w.fname = 'Marilyn'
        AND w.lname = 'Monroe'
GROUP BY pam.name
ORDER BY count(*) DESC;

D.6: (change worker/doctor accordingly)

SELECT 
    pam.name, count(*)
FROM
    Doctor as d
        JOIN
    Employees as e ON d.employees_Workers_wid = Workers_wid
        JOIN
    Workers as w ON e.Workers_wid = w.wid
        JOIN
    treatments as t ON d.employees_Workers_wid = t.Doctor_Employees_Workers_wid
        JOIN
    procedures_and_medications as pam ON t.procedures_and_medications_idprocedures_and_medications = pam.idprocedures_and_medications
        JOIN
    administers as a ON t.idTreatment = a.Treatments_idTreatment
        OR d.Employees_Workers_wid = a.Doctor_Employees_Workers_wid
WHERE
    w.fname = 'Marilyn'
        AND w.lname = 'Monroe'
GROUP BY pam.name
ORDER BY count(*) DESC;

D.7:

select 
    workers.lname, workers.fname
from
    workers
where
    wid in (select 
            Technician_Employees_Workers_wid
        from
            (select 
                count(Technician_Employees_Workers_wid) as t,
                    Technician_Employees_Workers_wid
            from
                inpatient_has_treatments
            join administers ON inpatient_has_treatments.Treatments_idTreatment = administers.Treatments_idTreatment
            group by Technician_Employees_Workers_wid) t2
        where
            t2.t = (select 
                    count(*)
                from
                    (select 
                        Patient_idPatient
                    from
                        inpatient
                    group by Patient_idPatient) t))
        OR wid in (select 
            Nurse_Employees_Workers_wid
        from
            (select 
                count(Nurse_Employees_Workers_wid) as t,
                    Nurse_Employees_Workers_wid
            from
                inpatient_has_treatments
            join administers ON inpatient_has_treatments.Treatments_idTreatment = administers.Treatments_idTreatment
            group by Nurse_Employees_Workers_wid) t2
        where
            t2.t = (select 
                    count(*)
                from
                    (select 
                        Patient_idPatient
                    from
                        inpatient
                    group by Patient_idPatient) t))
        Or wid in (select 
            Doctor_Employees_Workers_wid
        from
            (select 
                count(Doctor_Employees_Workers_wid) as t,
                    Doctor_Employees_Workers_wid
            from
                inpatient_has_treatments
            join administers ON inpatient_has_treatments.Treatments_idTreatment = administers.Treatments_idTreatment
            group by Doctor_Employees_Workers_wid) t2
        where
            t2.t = (select 
                    count(*)
                from
                    (select 
                        Patient_idPatient
                    from
                        inpatient
                    group by Patient_idPatient) t))
        OR wid in (select 
            Doctor_Employees_Workers_wid
        from
            (select 
                count(Doctor_Employees_Workers_wid) as t,
                    Doctor_Employees_Workers_wid
            from
                treatments
            join inpatient_has_treatments ON treatments.idTreatment = inpatient_has_treatments.Treatments_idTreatment
            group by Doctor_Employees_Workers_wid) t2
        where
            t2.t = (select 
                    count(*)
                from
                    (select 
                        Patient_idPatient
                    from
                        inpatient
                    group by Patient_idPatient) t));
